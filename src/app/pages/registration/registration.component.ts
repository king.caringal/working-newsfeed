import { Component, OnInit } from '@angular/core';
import { User } from '@models/user';
import { DataService } from '@services/data.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  user = new User;
  data: any;

  constructor(
    private dataservice: DataService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  submit(){
    this.dataservice.registerUser(this.user).subscribe(res => {
        this.data = res;
        this.router.navigate(['/login'])
    })
  }

}
