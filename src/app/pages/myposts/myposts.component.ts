import { Component, OnInit } from '@angular/core';


import { Post } from '@models/post';
import { PostService } from '@services/post.service';
import { SessionService } from '@services/session.service';

@Component({
  selector: 'app-myposts',
  templateUrl: './myposts.component.html',
  styleUrls: ['./myposts.component.css']
})
export class MypostsComponent implements OnInit {

  posts: Post[] = [];

  constructor(
    private sessionService: SessionService,
    private postService: PostService
  ) {
    this.getPosts();
   }

  ngOnInit(): void {
  }

  getPosts(){
    this.postService.get().subscribe((response : Post[]) =>{
        this.posts = response;
    })
  }
}
