import { Component, OnInit, Input } from '@angular/core';
import { Post } from '@models/post';
import { PostService } from '@services/post.service';
import { DataService } from '@services/data.service';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  hasToken: boolean = (localStorage.getItem('token') !== null);

  posts: Post[] = []

  constructor(

   private dataService: DataService,
    private postService: PostService
  ) {
    this.getPosts();
  }

  ngOnInit(): void {
  }

  getPosts(){
    this.dataService.getData().subscribe((response: Post[]) => {
      this.posts = response;
    })
  }

}
