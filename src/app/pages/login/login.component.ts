import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string = "";
  password: string = "";

  constructor(
    private userService: UserService,
    private router: Router,
    private sessionService: SessionService
  ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.userService.login(this.email,this.password).subscribe({
        next: this.successfulLogin.bind(this),
        error: this.failedLogin.bind(this)
    });
  }

  successfulLogin(response: Record<string,any>){
    this.sessionService.setEmail(response['email']);
    this.sessionService.setToken(response['token']);
    console.log("success")
    this.router.navigate(['/newsfeed'])
  }
  failedLogin(result: Record<string, any>){
    let data: Record<string,any> = result['error'];
    console.log("namo");
}
}
