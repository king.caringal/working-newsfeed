import { Component, OnInit } from '@angular/core';
import { Post } from '@models/post';
import { DataService } from '@services/data.service';

@Component({
  selector: 'app-newsfeed',
  templateUrl: './newsfeed.component.html',
  styleUrls: ['./newsfeed.component.css']
})
export class NewsfeedComponent implements OnInit {

  post = new Post;
  data: any;

  constructor(
    private dataservice: DataService,

  ) { }
  
  ngOnInit(): void {
  }

  postUser() {
    this.dataservice.postContent(this.post).subscribe(result => {
      this.data = result;
    })
  }
}
