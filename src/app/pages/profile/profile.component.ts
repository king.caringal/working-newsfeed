import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '@services/data.service';

import { Post } from '@models/post';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

id: any;
data:any;
post = new Post;
  

  constructor(
      private route: ActivatedRoute,
      private dataService: DataService,
  ) { }

  ngOnInit() {

  }



  url="./assets/cat.jpg";

  onselectFile(e){
    if(e.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload=(event: any)=>{
        this.url=event.target.result;
      }
    }
  }
}
