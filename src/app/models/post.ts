import { User } from "./user";

export class Post {

    constructor(
    public id?: number,
    public content?: string,
    public dateTimePosted?: Date,
    public user?: User

    ){}
}
