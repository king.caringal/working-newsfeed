export class User {
    constructor(
        public id?: number,
        public email?: string,
        public password?: string,
        public confirmedPass?: string,
        public firstName?: string,
        public lastName?: string,
        public gender?: any,
        public mobileNumber?: string,
       
    ) {}
}
