import { Component, Input, OnInit } from '@angular/core';
import { Post } from '@models/post';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  @Input() post!: Post

  hasToken: boolean = (localStorage.getItem('token') !==null);
  constructor() { }

  ngOnInit(): void {
  }

}
