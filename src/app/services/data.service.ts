import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { SessionService } from './session.service';
import { Post } from '@models/post';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private httpHeaders: HttpHeaders = new HttpHeaders({
    'Authorization': `Bearer ${this.sessionService.getToken()}`
  })

  constructor(
    private sessionService: SessionService,
    private httpClient: HttpClient
  ) { }

  registerUser(data){
    return this.httpClient.post('http://localhost:8080/api/users/register', data)
  }

  postContent(data: Post): Observable<Object> {
    return this.httpClient.post('http://localhost:8080/api/posts', data, {headers: this.httpHeaders})
  }

  getData(){
    return this.httpClient.get('http://localhost:8080/api/posts/timeline', {headers: this.httpHeaders})
  }

}