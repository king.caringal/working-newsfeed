import { Injectable } from '@angular/core';

import { environment } from '@environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '@models/post';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  
    private baseUrl: string = environment.apiUrl + '/posts';
    private httpHeaders: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${this.sessionService.getToken()}`
    }) // a class to be able to inlude the token while passing information
  

  constructor(
    private http: HttpClient,
    private sessionService: SessionService
  ) { }

  get(): Observable<Post[]>{
    return this.http.get<Post[]>(this.baseUrl);
  }
}
