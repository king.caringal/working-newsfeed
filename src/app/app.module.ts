import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { HomeComponent } from './pages/home/home.component';
import { NewsfeedComponent } from './pages/newsfeed/newsfeed.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { MypostsComponent } from './pages/myposts/myposts.component';
import { CardsComponent } from './components/cards/cards.component';
import { PostComponent } from './components/post/post.component';
import { PostsComponent } from './pages/posts/posts.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'newsfeed', component: NewsfeedComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'myProfile', component: ProfileComponent},
  {path: 'myposts', component: MypostsComponent},
  {path: 'posts', component: PostsComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegistrationComponent,
    HomeComponent,
    NewsfeedComponent,
    ProfileComponent,
    MypostsComponent,
    CardsComponent,
    PostComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
